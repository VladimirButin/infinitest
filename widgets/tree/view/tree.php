<?php

use yii\helpers\HtmlPurifier;

/**
 * @var $trees array
 */
?>

<?php if (!$nested): ?>
    <div class="tree-window" id="<?= $id ?>">
<?php endif; ?>

    <?php foreach ($trees as $tree): ?>

        <div class="tree-container" data-id="<?= (int) $tree->id ?>">
            <div class="tree-header">
                <a href="#" class="tree-expand"></a>
                <a href="#" class="tree-link">
                    <?= HtmlPurifier::process($tree->name); ?>
                </a>
                (<?= $tree->getChildrenCount() ?> children, <?= $tree->getDescendantsCount() ?> descendants)
                
                <span>[<a href="#" class="tree-new-child">New child</a>]</span>
                <span>[<a href="#" class="tree-make-root">Make root</a>]</span>
                <span>[<a href="#" class="tree-move">Move</a>]</span>
                <span>[<a href="#" class="tree-delete">Delete</a>]</span>
                <span>[<a href="#" class="tree-takeout">Take out</a>]</span>
            </div>
            <div class="tree-children">

            </div>
        </div>

    <?php endforeach; ?>

<?php if (!$nested): ?>
    </div>
<?php endif; ?>