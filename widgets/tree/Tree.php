<?php

namespace app\widgets\tree;

use yii\base\Widget;

class Tree extends Widget {

    /**
     * Array of trees
     * @var array
     */
    public $trees = [];

    /**
     * Do we need to render outer container
     * If false - yes, if 
     * 
     * @var bool 
     */
    public $nested = false;

    public function run() {
        return $this->render('@app/widgets/tree/view/tree', [
                    'trees' => $this->trees,
                    'nested' => $this->nested,
                    'id' => $this->id,
        ]);
    }

}
