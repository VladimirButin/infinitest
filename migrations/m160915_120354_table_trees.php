<?php

use yii\db\Migration;

class m160915_120354_table_trees extends \yii\db\Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%trees}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->null(),
            'sort' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->notNull()
                ], $tableOptions);

        $this->createIndex('parent_sort', '{{%trees}}', ['parent_id', 'sort']);
    }

    public function down() {
        $this->dropTable('{{%trees}}');
    }

}
