<?php

use yii\db\Migration;
use app\models\Tree;

class m160915_125020_generate_trees extends Migration {

    private $_counter = 0;

    /**
     * Generate single node
     * @return Tree
     */
    private function generateNode() {
        $this->_counter++;

        return new Tree([
            'name' => 'Node ' . $this->_counter,
            'description' => 'Description of node ' . $this->_counter,
            'parent_id' => null,
            'sort' => 0,
        ]);
    }

    /**
     * Generate tree
     */
    private function generateTree() {
        $suitableNodes = [];
        $nodesAmount = rand(Yii::$app->params['treeGeneration']['nodesMin'], Yii::$app->params['treeGeneration']['nodesMax']) - 1;

        $node = $this->generateNode();
        $node->save();
        $suitableNodes[] = $node;

        for ($nodesAmount; $nodesAmount > 0; $nodesAmount--) {
            $node = $this->generateNode();
            $parentElement = $suitableNodes[rand(1, count($suitableNodes)) - 1];
            $node->parent_id = $parentElement->id;
            $node->save();
            $suitableNodes[] = $node;

            for ($i = count($suitableNodes) - Yii::$app->params['treeGeneration']['lastSuitable']; $i > 0; $i--) {
                array_shift($suitableNodes);
            }
        }
    }

    public function up() {
        $treesAmount = rand(Yii::$app->params['treeGeneration']['amountMin'], Yii::$app->params['treeGeneration']['amountMax']);

        for ($treesAmount; $treesAmount > 0; $treesAmount--) {
            $this->generateTree();
        }
    }

    public function down() {
        $this->delete('{{%trees}}');
    }

}
