<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Tree;

class TreeController extends Controller {

    /**
     * Get all roots
     * 
     * @return type
     */
    public function actionRoots() {
        $trees = Tree::getRoots();

        return $this->renderPartial('children', ['trees' => $trees]);
    }

    /**
     * Get all children by node
     * 
     * @param type $id
     * @return type
     */
    public function actionChildren($id = null) {
        $root = $this->getNode($id);
        $trees = $root->getChildrenNodes();

        return $this->renderPartial('children', ['trees' => $trees]);
    }

    /**
     * Get node data
     *
     * @param type $id
     * @return type
     * @throws Exception
     */
    public function actionNodeData($id = null) {
        $node = $this->getNode($id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'name' => $node->name,
            'description' => $node->description,
            'id' => $node->id,
            'parent_id' => $node->parent_id,
        ];
    }

    /**
     * Validate node data
     * 
     * @return type
     */
    public function actionValidate() {
        $node = new Tree();

        if (Yii::$app->request->isPost && $node->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($node);
        }
    }

    /**
     * Update node data and render node widget
     *
     * @return type
     */
    public function actionUpdate() {
        $node = new Tree();

        if (Yii::$app->request->isPost && $node->load(Yii::$app->request->post())) {
            if ($node->id) {
                $node = $this->getNode($node->id);
                $node->load(Yii::$app->request->post());
            }
            $node->save();

            return $this->renderPartial('children', ['trees' => [$node]]);
        }
    }

    /**
     * Insert node and render node widget
     *
     * @return type
     */
    public function actionInsert() {
        $node = new Tree();

        if (Yii::$app->request->isPost && $node->load(Yii::$app->request->post())) {
            $node->save();

            return $this->renderPartial('children', ['trees' => [$node]]);
        }
    }

    /**
     * Make node root
     *
     * @return type
     */
    public function actionMakeRoot($id) {
        $node = $this->getNode($id);
        $node->makeNodeRoot()->save();

        return $this->renderPartial('children', ['trees' => [$node]]);
    }

    /**
     * Delete node
     *
     * @return type
     */
    public function actionDelete($id) {
        $node = $this->getNode($id);
        $node->deleteWithChildren();

        return true;
    }

    /**
     * Delete node
     *
     * @return type
     */
    public function actionTakeout($id) {
        $node = $this->getNode($id);
        $trees = $node->takeout();

        return $this->renderPartial('children', ['trees' => $trees]);
    }
    
    /**
     * Move node
     * 
     * @param int $id
     * @param int $toId
     * @return boolean
     */
    public function actionMove($id, $toId) {
        $node = $this->getNode($id);
        $node->parent_id = $toId;
        $node->save();
        
        return true;
    }

    /**
     * Get node by id
     * 
     * @param int $id
     * @return Tree
     * @throws Exception
     */
    private function getNode($id) {
        $node = Tree::findOne($id);
        if (empty($node)) {
            throw new Exception('Node ' . $id . ' data is missing', 404);
        }

        return $node;
    }

}
