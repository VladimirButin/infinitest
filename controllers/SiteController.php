<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Tree;

class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return ['error' => ['class' => 'yii\\web\\ErrorAction']];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $trees = Tree::getRoots();
        $formTree = new Tree();

        return $this->render('index', [
                    'formTree' => $formTree,
                    'trees' => $trees,
        ]);
    }

}
