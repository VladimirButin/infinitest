$(function () {
    var $treeWidget = $('#treeWdiget');

    /*
     * Children view
     */
    $(document).on('click', '.tree-expand', function () {
        var $container = $(this).closest('.tree-container');

        if ($container.hasClass('waiting')) {
            return false;
        }

        if ($container.hasClass('opened')) {
            $container.removeClass('opened');
            $container.children('.tree-children').empty();
        } else {
            $container.addClass('waiting');

            $.ajax({
                url: 'tree/children',
                data: {
                    id: $container.data('id')
                },
                method: 'GET',
                dataType: 'html'
            }).then(function (result) {
                $container.children('.tree-children').html(result);
                $container.removeClass('waiting').addClass('opened');
            });
        }

        return false;
    });

    /*
     * Node view, insert and edit
     */

    var FORM_MODE_UPDATE = 1,
            FORM_MODE_CREATE_ROOT = 2,
            FORM_MODE_CREATE_CHILD = 3;

    var $editModal = $('#editModal')
            , $nodeForm = $('#nodeForm')
            , formMode;

    /**
     * Update node after saving
     * @returns {undefined}
     */
    function updateNode() {
        var id = $editModal.find('#tree-id').val();
        var name = $editModal.find('#tree-name').val();

        $('.tree-container[data-id=' + id + ']').children('.tree-header').children('.tree-link').html(name);
    }

    /**
     * Create child after saving
     * @returns {undefined}
     */
    function createChild(data) {
        var parent_id = $editModal.find('#tree-parent_id').val();

        $('.tree-container[data-id=' + parent_id + ']').children('.tree-children').append(data);
    }

    /**
     * Create root after saving
     * @returns {undefined}
     */
    function createRoot(data) {
        $treeWidget.append(data);
    }

    $nodeForm.on('beforeSubmit', function () {
        var url = formMode === FORM_MODE_UPDATE ? 'tree/update' : 'tree/insert';

        $.ajax({
            url: url,
            data: $(this).serialize(),
            method: 'POST',
            dataType: 'html'
        }).then(function (result) {
            switch (formMode) {
                case FORM_MODE_UPDATE:
                    updateNode();
                    break;
                case FORM_MODE_CREATE_CHILD:
                    createChild(result);
                    break;
                case FORM_MODE_CREATE_ROOT:
                    createRoot(result);
                    break;
            }

            $editModal.modal('hide');
        });

        return false;
    });

    // Tree link handler
    $(document).on('click', '.tree-link', function () {
        var $container = $(this).closest('.tree-container');

        if (moving) {
            $.ajax({
                url: 'tree/move',
                data: {
                    id: $movedContainer.data('id'),
                    toId: $container.data('id')
                },
                method: 'GET',
                dataType: 'html'
            }).then(function () {
                $container.children('.tree-children').append($movedContainer);
                cancelMoving();
            });

            return false;
        }

        $.ajax({
            url: 'tree/node-data',
            data: {
                id: $container.data('id')
            },
            method: 'GET',
            dataType: 'json'
        }).then(function (result) {
            $editModal.find('#tree-id').val(result.id);
            $editModal.find('#tree-name').val(result.name);
            $editModal.find('#tree-description').val(result.description);
            $editModal.find('#tree-parent_id').val(result.parent_id);
            $editModal.modal('show');

            formMode = FORM_MODE_UPDATE;
        });

        return false;
    });

    // New child handler
    $(document).on('click', '.tree-new-child', function () {
        var $container = $(this).closest('.tree-container');

        $editModal.find('#tree-id').val('');
        $editModal.find('#tree-name').val('');
        $editModal.find('#tree-description').val('');
        $editModal.find('#tree-parent_id').val($container.data('id'));
        $editModal.modal('show');

        formMode = FORM_MODE_CREATE_CHILD;

        return false;
    });

    // New root handler
    $(document).on('click', '#newRoot', function () {
        $editModal.find('#tree-id').val('');
        $editModal.find('#tree-name').val('');
        $editModal.find('#tree-description').val('');
        $editModal.find('#tree-parent_id').val('');
        $editModal.modal('show');

        formMode = FORM_MODE_CREATE_ROOT;

        return false;
    });

    /*
     * Roots reload
     */

    $('#refresh').click(function () {
        $.ajax({
            url: 'tree/roots',
            method: 'GET',
            dataType: 'html'
        }).then(function (result) {
            $treeWidget.empty();
            $treeWidget.html(result);
        });

        return false;
    });

    /*
     * Make node a root
     */
    $(document).on('click', '.tree-make-root', function () {
        var $container = $(this).closest('.tree-container');

        $.ajax({
            url: 'tree/make-root',
            method: 'GET',
            dataType: 'html',
            data: {
                id: $container.data('id')
            }
        }).then(function () {
            $container.remove();
            $container.appendTo($treeWidget);
        });

        return false;
    });

    /*
     * Delete node
     */
    $(document).on('click', '.tree-delete', function () {
        var $container = $(this).closest('.tree-container');

        $.ajax({
            url: 'tree/delete',
            method: 'GET',
            dataType: 'html',
            data: {
                id: $container.data('id')
            }
        }).then(function () {
            $container.remove();
        });

        return false;
    });

    /*
     * Take out node
     */
    $(document).on('click', '.tree-takeout', function () {
        var $container = $(this).closest('.tree-container');

        $.ajax({
            url: 'tree/takeout',
            method: 'GET',
            dataType: 'html',
            data: {
                id: $container.data('id')
            }
        }).then(function (result) {
            $container.replaceWith(result);
        });

        return false;
    });

    var moving = false
            , $cancelMovingBt = $('#cancelMovingBt')
            , $movedContainer;

    function cancelMoving() {
        moving = false;
        $movedContainer.removeClass('moving');
        $movedContainer = null;
        $cancelMovingBt.addClass('hidden');
    }

    $(document).on('click', '.tree-move', function () {
        $movedContainer = $(this).closest('.tree-container');
        moving = true;
        $cancelMovingBt.removeClass('hidden');
        $movedContainer.addClass('moving');
    });

    $cancelMovingBt.click(cancelMoving);

});
    