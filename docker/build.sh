#!/bin/bash

cd php
docker build -t infinitest-php .
cd ..

cd mysql
docker build -t infinitest-mysql .
cd ..