<?php

namespace app\models;

use Yii;
use paulzi\adjacencyList\AdjacencyListBehavior;
use paulzi\autotree\AutoTreeTrait;

/**
 * This is the model class for table "trees".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $sort
 * @property string $name
 * @property string $description
 */
class Tree extends \yii\db\ActiveRecord {

    use AutoTreeTrait;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => AdjacencyListBehavior::className(),
                'sortable' => false,
                'checkLoop' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%trees}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'parent_id', 'sort'], 'integer'],
            [['sort', 'name', 'description'], 'required'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'sort' => 'Sort',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate() {
        if (parent::beforeValidate()) {
            if (empty($this->sort)) {
                $this->sort = 0;
            }

            if (empty($this->parent_id)) {
                $this->makeRoot();
            } else {
                $parent = self::findOne($this->parent_id);
                if (!$parent) {
                    return false;
                }
                $this->appendTo($parent);
            }

            return true;
        }

        return false;
    }

    /**
     * Returns all roots of trees
     * 
     * @return array
     */
    static public function getRoots() {
        return self::find()->where(['parent_id' => null])->all();
    }

    /**
     * Get all children of node
     *
     * @return array
     */
    public function getChildrenNodes() {
        return $this->getChildren()->all();
    }

    /**
     * Get count of children
     * 
     * @return int
     */
    public function getChildrenCount() {
        return $this->getChildren()->count();
    }

    /**
     * Get count of descendants
     * 
     * @return int
     */
    public function getDescendantsCount() {
        return $this->getDescendants()->count();
    }

    /**
     * Make node root
     * 
     * @return Tree
     */
    public function makeNodeRoot() {
        $this->parent_id = null;
        return $this;
    }

    /**
     * Delete node and take out it's children
     *
     * @return array
     */
    public function takeout() {
        $children = $this->getChildrenNodes();
        
        foreach ($children as $child) {
            if ($this->parent_id) {
                $child->parent_id = $this->parent_id;
            } else {
                $child->parent_id = null;
            }
            
            $child->save();
        }
        
        $this->deleteWithChildren();
        return $children;
    }

}
