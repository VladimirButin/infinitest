<?php

/* @var $this yii\web\View */

use app\widgets\tree\Tree;
?>

<?= Tree::widget(['nested' => true, 'trees' => $trees]); ?>