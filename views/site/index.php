<?php
/* @var $this yii\web\View */
/* @var $trees array */
/* @var $formTree app\models\Tree */

use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\tree\Tree;

$this->title = 'Test application';
?>

<div class="row">
    <?= Html::button('Make new root', ['class' => 'btn btn-primary', 'id' => 'newRoot']) ?>
    <?= Html::button('Refresh', ['class' => 'btn btn-default', 'id' => 'refresh']) ?>
    <?= Html::button('Cancel moving', ['class' => 'btn btn-danger hidden', 'id' => 'cancelMovingBt']) ?>
</div>

<div class="row" >
    <div class="well">
        <?= Tree::widget(['trees' => $trees, 'id' => 'treeWdiget']); ?>
    </div>
</div>
<?php
Modal::begin([
    'id' => 'editModal',
    'header' => '<h3>Tree node edit</h3>',
]);
?>

<?php
$form = ActiveForm::begin([
            'action' => 'tree/update',
            'enableAjaxValidation' => true,
            'validationUrl' => 'tree/validate',
            'options' => [
                'id' => 'nodeForm',
            ],
        ]);
?>

<?= $form->field($formTree, 'name') ?>

<?= $form->field($formTree, 'description') ?>

<?= $form->field($formTree, 'id', ['template' => '{input}'])->hiddenInput(); ?>

<?= $form->field($formTree, 'parent_id', ['template' => '{input}'])->hiddenInput(); ?>


<div class="form-group text-right">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    <?= Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
</div>

<?php $form->end(); ?>

<?php Modal::end(); ?>