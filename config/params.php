<?php

return [
    'treeGeneration' => [
        'amountMin' => 3, // Minimal amount of trees
        'amountMax' => 3, // Maximal amount of trees
        'lastSuitable' => 10, // Use last amount of point to join new point
        'nodesMin' => 200, // Minimal amount of elements per tree
        'nodesMax' => 300, // Maximal amount of elements per tree
    ]
];
