<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'paulzi/yii2-auto-tree' => 
  array (
    'name' => 'paulzi/yii2-auto-tree',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@paulzi/autotree' => $vendorDir . '/paulzi/yii2-auto-tree',
    ),
  ),
  'paulzi/yii2-sortable' => 
  array (
    'name' => 'paulzi/yii2-sortable',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@paulzi/sortable' => $vendorDir . '/paulzi/yii2-sortable',
    ),
  ),
  'paulzi/yii2-adjacency-list' => 
  array (
    'name' => 'paulzi/yii2-adjacency-list',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@paulzi/adjacencyList' => $vendorDir . '/paulzi/yii2-adjacency-list',
    ),
  ),
);
